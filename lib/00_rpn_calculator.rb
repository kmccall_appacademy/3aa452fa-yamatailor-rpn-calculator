class RPNCalculator
  attr_accessor :calculator

  def initialize
    @stack = []
    @count = 0
  end

  def value
    @stack[-1]
  end

  def push(num)
    @stack << num
    @count += 1
  end

  def plus
    self.error_check
    first_num = @stack.pop
    second_num = @stack.pop
    @stack << first_num + second_num
    @count -= 1
  end

  def minus
    self.error_check
    first_num = @stack.pop
    second_num = @stack.pop
    @stack << second_num - first_num
    @count -= 1
  end

  def times
    self.error_check
    first_num = @stack.pop
    second_num = @stack.pop
    @stack << first_num * second_num
    @count -= 1
  end

  def divide
    self.error_check
    first_num = @stack.pop
    second_num = @stack.pop
    @stack << second_num.to_f / first_num.to_f
    @count -= 1
  end

  def tokens(string)
    operators = ['+', '-', '*', '/']
    string.split.map do |el|
      operators.include?(el) ? el.to_sym : el.to_i
    end
  end

  def evaluate(string)
    self.tokens(string).each do |el|
      if el == :+
        self.plus
      elsif el == :-
        self.minus
      elsif el == :*
        self.times
      elsif el == :/
        self.divide
      else
        self.push(el)
      end
    end
    self.value
  end

  def error_check
    raise "calculator is empty" if @count < 2
  end
end
